package com.example.to4ka.tree

import com.example.to4ka.tree.TreeNode.isSameTree
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class TreeNodeSpec extends AnyFlatSpec with Matchers {

  "TreeNodes" should "be the same" in {
    val simpleP = TreeNode(1, None, None)
    val simpleQ = TreeNode(1, None, None)
    assert(isSameTree(Some(simpleP), Some(simpleQ)))
  }

  "TreeNodes" should "be different" in {
    val p = TreeNode(1, Some(TreeNode(2, None, None)), None)
    val q = TreeNode(1, None, Some(TreeNode(2, None, None)))
    assert(!isSameTree(Some(p), Some(q)))
  }

}
