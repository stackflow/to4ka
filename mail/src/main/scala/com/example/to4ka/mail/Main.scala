package com.example.to4ka.mail

import java.net.InetAddress
import scala.io.StdIn.readLine
import scala.util.matching.Regex

object Main {
  def main(args: Array[String]): Unit = {
    print("Please, enter your email: ")
    val input = readLine()
    if (verifyMail(input)) {
      println("Thanks, your email is valid")
    } else {
      println("Sorry, you enter wrong email")
    }
  }

  val mailPattern: Regex = "^[A-Za-z0-9+_.-]+@[A-Za-z0-9.-]+$".r

  def verifyMail(input: String): Boolean = {
    mailPattern.matches(input)
  }
}
