package com.example.to4ka

package object symbol {

  implicit class StringOops(val str: String) extends AnyVal {
    def sortSymbols: String = {
      str.groupBy(identity).values.toSeq.sortBy(-1 * _.length).mkString
    }
  }

}
