package com.example.to4ka.tree

case class TreeNode[X](value: X, left: Option[TreeNode[X]], right: Option[TreeNode[X]])

object TreeNode {

  def isSameTree[X](p: Option[TreeNode[X]], q: Option[TreeNode[X]]): Boolean = {
    (p, q) match {
      case (None, None) =>
        true
      case (_, None) =>
        false
      case (None, _) =>
        false
      case (Some(ptn), Some(qtn)) =>
        ptn.value == qtn.value && isSameTree(ptn.left, qtn.left) && isSameTree(ptn.right, qtn.right)
    }
  }

}
