package com.example.to4ka.mail

import com.example.to4ka.mail.Main.verifyMail
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class MainSpec extends AnyFlatSpec with Matchers {

  "Mail verifier" should "pass valid emails" in {
    assert(verifyMail("alice@example.com"))
    assert(verifyMail("alice.bob@example.com"))
    assert(verifyMail("alice@example.me.org"))
  }

  "Mail verifier" should "not pass wrong emails" in {
    assert(!verifyMail("alice.example.com"))
    assert(!verifyMail("alice#@example.com"))
    assert(!verifyMail("@example.me.org"))
  }

}
