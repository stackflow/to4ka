ThisBuild / organization := "com.example"
ThisBuild / version := "0.1"
ThisBuild / scalaVersion := "2.13.5"

lazy val scalaTest = Seq(
  "org.scalatest" %% "scalatest" % "3.2.7" % Test
)
lazy val commonDependencies = scalaTest

lazy val root = (project in file("."))
  .aggregate(symbol, tree, mail)

lazy val symbol = (project in file("symbol"))
  .settings(
    libraryDependencies ++= commonDependencies,
  )

lazy val tree = (project in file("tree"))
  .settings(
    libraryDependencies ++= commonDependencies,
  )

lazy val mail = (project in file("mail"))
  .settings(
    libraryDependencies ++= commonDependencies,
  )