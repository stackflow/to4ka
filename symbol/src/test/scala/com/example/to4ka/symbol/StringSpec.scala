package com.example.to4ka.symbol

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class StringSpec extends AnyFlatSpec with Matchers {

  behavior of "String"

  it should "be updated properly" in {
    val t = "asdassdaaaweqbbbbasddassd".sortSymbols
    assert(t.startsWith("aaaaaaassssssdddddbbbb"))
  }

}
